from django.contrib import admin
from django.contrib.admin import site

from .models import Post, Feedback

site.register(Feedback)
admin.site.register(Post)
