# coding=utf-8
from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import TemplateView

from socialn.forms import FeedbackForm
from socialn.models import Feedback

# сделать чат

# !_!_!_!_!

class HomeView(TemplateView):
    template_name = 'index.html'

class ProfilePage(TemplateView):
    template_name = 'registration/profile.html'


class RegisterView(TemplateView):
    template_name = 'register.html'

class AuthView(TemplateView):
    template_name = 'registration/auth.html'

class FeedBack(TemplateView):
    template_name = 'feedback.html'
    def dispatch(self, request, *args, **kwargs):

        context ={}
        form = FeedbackForm()
        if request.method == "POST":
            form = FeedbackForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, u"Ваш отзыв принят")
                return redirect(reverse("feedback"))

        context['feedback_form'] = form
        return render(request, self.template_name, context)


class ViewFeedBack(TemplateView):
    template_name = 'view-feedback.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse('home'))
        context ={
            'feedbacks': Feedback.objects.all()
        }
        return render(request, self.template_name, context)

